#!/usr/bin/env bash

#########################################################################
#
# Script to source this repo's rc files to the users profile
#
# Usage: ./sourceall.sh
#
#########################################################################

CURDIR=$(pwd)

for FILE in "vimrc" "bashrc" "screenrc"
do
    SOURCEFILE="source ${CURDIR}/${FILE}"

    if [[ -e ~/.${FILE} ]]
    then
        IS_SOURCED=$(grep "${SOURCEFILE}" ~/.${FILE})
    else
        unset IS_SOURCED
    fi

    if [[ -z ${IS_SOURCED} ]]
    then
        echo "${SOURCEFILE}" >> ~/.${FILE}
        echo "Sourced ~/.${FILE}"
    fi
done
