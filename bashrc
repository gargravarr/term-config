# Auxiliary functions
current_kube() {
    echo $(cat ~/.kube/config 2> /dev/null | grep current-context |  cut -d \  -f 2)
}

git_branch() {
     echo $(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
}

## Prompt

export PS1='\D{%y/%m/%d %H:%M} [\[\e[0;36m\]$(current_kube)\[\e[0m\]|\[\e[0;32m\]$(git_branch)\[\e[0m\]|\W]\$ '

## Kubernetes
export EDITOR=vim
