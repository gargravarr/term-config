" Display syntax, line number and right margin limit. Define invisible chars.
" Display cursor line
" Always display bottom status line
syntax on
set number
set cc=73,80,120
set list
set listchars=tab:>_,trail:.,eol:¶
set cursorline
set laststatus=2
set ruler

" Viewing options

" Colors: Invisible characters and right margin limit.
"colorscheme pablo
highlight NonText ctermfg=0
highlight SpecialKey ctermfg=0
highlight ColorColumn ctermbg=0
highlight LineNr ctermfg=6 ctermbg=0

" Panes
set cmdheight=2

" Tab and indentation set to 4. Convert tabs to spaces
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL

" In ~/.vim/vimrc, or somewhere similar.
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'go': ['gofmt'],
\   'python': ['black', 'isort'],
\
\}

" Set this variable to 1 to fix files when you save them.
let g:ale_fix_on_save = 1
